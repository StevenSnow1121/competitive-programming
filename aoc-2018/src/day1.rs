use std::collections::HashMap;

#[aoc_generator(day1)]
pub fn input_generator(input: &str) -> Vec<i32> {
    input.lines().map(|l| l.parse().unwrap()).collect()
}

#[aoc(day1, part1)]
pub fn part1(input: &[i32]) -> i32 {
    let mut res = 0;

    for i in input {
        res += i;
    }

    res // 411
}

#[aoc(day1, part2)]
pub fn part2(input: &[i32]) -> i32 {
    let mut res = 0;
    let mut seen: HashMap<i32, bool> = HashMap::new();

    for i in input.iter().cycle() {
        res += i;
        if let std::collections::hash_map::Entry::Vacant(e) = seen.entry(res) {
            e.insert(true);
        } else {
            return res; // 56360
        }
    }
    0
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn part1_example() {
        assert_eq!(part1(&[-1, -2, -3]), -6);
    }

    #[test]
    fn part2_example() {
        assert_eq!(part2(&[3, 3, 4, -2, -4]), 10);
    }
}
