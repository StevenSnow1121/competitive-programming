package nl.arjenwiersma.aoc.days;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import nl.arjenwiersma.aoc.common.Day;
import nl.arjenwiersma.aoc.common.DayInputExternalResource;

public class Day17Test {
    @Rule
    public DayInputExternalResource input = new DayInputExternalResource(17);

    Day17 day;
    
    @Before
    public void before() {
        day = new Day17();
    }

    @Test
    public void testSample() {
        List<String> input = new ArrayList<>() {{
                add("target area: x=20..30, y=-10..-5");
            }
        };
        assertEquals(45, (int) day.part1(input));
        assertEquals(112, (int) day.part2(input));
    }

    @Test
    public void part1() {
        assertEquals(12246, (int) day.part1(input.getLines()));
    }
    
    @Test
    public void part2() {
        assertEquals(3528, (int) day.part2(input.getLines()));
    }
}
