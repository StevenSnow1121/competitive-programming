package nl.arjenwiersma.aoc.days;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;

public class Snailfish {
    Long value = null;

    Snailfish root;
    
    Snailfish left;
    Snailfish right;

    public Snailfish() {
        
    }

    public Snailfish(long value) {
        this.value = value;
    }


    public Snailfish(Snailfish left, Snailfish right) {
        this.left = left;
        this.right = right;
        left.root = this;
        right.root = this;
    }

    @Override
    public String toString() {
        if (isLiteral())
            return value.toString();
        return "[" + left.toString() + "," + right.toString() + "]";
    }

    public static Snailfish parse(String input) {
        List<Character> c = new ArrayList<>();
        for (char ca : input.toCharArray()) {
            c.add(Character.valueOf(ca));
        }
        return parse(c, null);
    }
    
    public static Snailfish parse(List<Character> input, Snailfish root) {
        Snailfish sf = new Snailfish();
        sf.root = root;

        // [1,2] -> first char is [, means this is a root of a tree
        if (input.get(0) == '[') {
            input.remove(0);
            sf.left = parse(input, sf);
            // after parsing all the node data, the ',' should seperate left and right
            if (input.get(0) != ',') {
                System.out.println("After parsing the current pos is not a ,:" + input);
            }
            input.remove(0);
            sf.right = parse(input, sf);
            input.remove(0); // drop the last ]
            return sf;
        } 

        // if not a tree structure, '[', we should be at a literal value
        List<Character> consume = new ArrayList<>();
        // consume until , or ]
        boolean more = true;
        while (more) {
            consume.add(input.remove(0));
            if (input.get(0) == ',' || input.get(0) == ']') {
                // all chars read
                String s = consume.stream().collect(Collector.of(StringBuilder::new,
                                                                 StringBuilder::append,
                                                                 StringBuilder::append,
                                                                 StringBuilder::toString));

                sf.value = Long.valueOf(s);
                more = false;
            }
        }
        
        return sf;
    }

    protected Snailfish clone() {
        if (isLiteral())
            return new Snailfish(value);
        return new Snailfish(left.clone(), right.clone());
    }

    public static Snailfish add(Snailfish left, Snailfish right) {
        Snailfish newRoot = new Snailfish();
        newRoot.left = left;
        newRoot.right = right;
        left.root = newRoot;
        right.root = newRoot;

        newRoot.reduce();
        return newRoot;
    }

    public Snailfish add(Snailfish other) {
        Snailfish newRoot = new Snailfish();
        newRoot.left = this;
        newRoot.right = other;
        this.root = newRoot;
        other.root = newRoot;

        newRoot.reduce();
        
        return newRoot;
    }

    private boolean isLiteral() {
        return value != null;
    }
    
    private int getDepth() {
        if (root != null)
            return root.getDepth() + 1;
        return 0;
    }

    private Snailfish getTreeRoot() {
        Snailfish c = this;
        while (c.root != null)
            c = c.root;
        return c;
    }

    public void reduce() {
        while (explode() || split());
    }

    private List<Snailfish> digits() {
        if (isLiteral()) return List.of(this);
        List<Snailfish> all = new ArrayList<>();
        all.addAll(left.digits());
        all.addAll(right.digits());
        return all;
    }

    private Snailfish nearestSnailfish(Snailfish current, int direction) {
        List<Snailfish> digits = getTreeRoot().digits();
        int i = digits.indexOf(current) + direction;
        if (i < 0 || i >= digits.size())
            return null;
        return digits.get(i);
    }

    private boolean explode() {
        //System.out.println("Explode! " + this + " literal: " + isLiteral());
        if (!isLiteral()) {
            //System.out.println("Evaling: " + this);
            if (getDepth() == 4) {
                //System.out.println("Exploding: " + this);
                Snailfish sfl = nearestSnailfish(left, -1);
                if (sfl != null)
                    sfl.value += left.value;
                
                Snailfish sfr = nearestSnailfish(right, 1);
                if (sfr != null)
                    sfr.value += right.value;

                left = null;
                right = null;
                value = 0L;
                return true;    // return true to allow split to execute
            } else {
                return left.explode() || right.explode();
            }
        }
        return false;
    }
    
    private boolean split() {
        if (isLiteral()) {
            if (value >= 10) {
                left = new Snailfish();
                left.value = value / 2;
                right = new Snailfish();
                right.value = value / 2 + value % 2;

                left.root = this;
                right.root = this;

                value = null;
                return true;
            }
        } else {
            return left.split() || right.split();
        }
        return false; 
    }

    public long magnitude() {
        if (isLiteral()) return value;
        return 3 * left.magnitude() + 2 * right.magnitude();
    }
}
