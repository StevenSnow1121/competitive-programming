package nl.arjenwiersma.aoc.days;

import static nl.arjenwiersma.aoc.utils.SlidingWindowSpliterator.windowed;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import nl.arjenwiersma.aoc.common.Day;

public class Day14 implements Day<Long> {
    private Long generatePolymer(List<String> input, int iterations) {
        String template = input.get(0);

        Map<String, Character> rules = new HashMap<>();

        input.stream().skip(2).forEach(x -> {
                String[] parts = x.split(" -> ");
                rules.put(parts[0].trim(), parts[1].charAt(0));
        });
        
        Map<String, Long> currentPairs = new HashMap<>();

        List<List<Character>> win = windowed(template.chars()
                           .mapToObj(x -> Character.valueOf((char) x))
                           .toList(), 2)
            .map(s -> s.collect(Collectors.toList()))
            .toList();

        for (List<Character> c : win) {
            currentPairs.merge("" + c.get(0) + c.get(1), 1L, Long::sum);
        }

        // Count the occurrence of each new character
        Map<Character, Long> counts = new HashMap<>();
        template.chars().forEach(x -> counts.merge((char) x, 1L, Long::sum));

        for (int i = 0; i < iterations; i++) {
            // copy of the current pairs map
            Map<String, Long> t = new HashMap<>(currentPairs);

            for (String s : t.keySet()) {
                char r = rules.get(s);
                char a = s.charAt(0);
                char b = s.charAt(1);

                // get the value from the previous situation
                long incr = t.getOrDefault("" + a + b, 1L);
                // Update the current one
                currentPairs.merge("" + a + b, -incr, Long::sum); // remove one pair from the list
                currentPairs.merge("" + a + r, incr, Long::sum);
                currentPairs.merge("" + r + b, incr, Long::sum);

                // keep track of the new characters
                counts.merge(r, incr, Long::sum);
            }
        }

        long max = counts.entrySet().stream().mapToLong(x -> x.getValue()).max().getAsLong();
        long min = counts.entrySet().stream().mapToLong(x -> x.getValue()).filter(x -> x > 0).min().getAsLong();
        return max - min;
    }

    @Override
    public Long part1(List<String> input) {
        return generatePolymer(input, 10);
    }

    @Override
    public Long part2(List<String> input) {
        return generatePolymer(input, 40);
    }    
}
