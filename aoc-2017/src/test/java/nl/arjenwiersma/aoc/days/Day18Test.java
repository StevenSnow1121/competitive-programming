package nl.arjenwiersma.aoc.days;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import nl.arjenwiersma.aoc.common.DayInputExternalResource;

/**
 * Day18Test
 */
public class Day18Test {
    
    @Rule
    public DayInputExternalResource input = new DayInputExternalResource(18);

    Day18 day;
    
    @Before
    public void before() {
        day = new Day18();
    }

    @Test
    public void testSampleOne() {
        List<String> in = new ArrayList<>() {{
                add("set a 1");
                add("add a 2");
                add("mul a a");
                add("mod a 5");
                add("snd a");
                add("set a 0");
                add("rcv a");
                add("jgz a -1");
                add("set a 1");
                add("jgz a -2");
            }
        };

        assertEquals(4, (long) day.part1(in));
    }

    @Test
    public void part_one() {
        assertEquals(4601, (long) day.part1(input.getLines()));
    }

    @Test
    public void testSampleTwo() {
        List<String> in = new ArrayList<>() {{
                add("snd 1");
                add("snd 2");
                add("snd p");
                add("rcv a");
                add("rcv b");
                add("rcv c");
                add("rcv d");
            }
        };

        assertEquals(3, (long) day.part2(in));
    }

    @Test
    public void part_two() {
        assertEquals(6858, (long) day.part2(input.getLines()));
    }
    
}
