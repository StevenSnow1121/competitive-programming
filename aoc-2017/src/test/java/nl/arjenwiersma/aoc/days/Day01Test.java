package nl.arjenwiersma.aoc.days;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import nl.arjenwiersma.aoc.common.DayInputExternalResource;

public class Day01Test {
    @Rule
    public DayInputExternalResource input = new DayInputExternalResource(01);

    Day01 day;
    
    @Before
    public void before() {
        day = new Day01();
    }

    @Test
    public void testSample() {
        Map<String, Integer> data = new HashMap<>() {{
                put("1122", 3);
                put("1111", 4);
                put("1234", 0);
                put("91212129", 9);
            }
        };

        for (String k : data.keySet()) {
            assertEquals(data.get(k), day.part1(List.of(k)));
        }
    }

    @Test
    public void part_one() {
        assertEquals(1141, (int) day.part1(input.getLines()));
    }
    
    @Test
    public void testSampleTwo() {
        Map<String, Integer> data = new HashMap<>() {
            {
                put("1212", 6);
                put("1221", 0);
                put("123425", 4);
                put("123123", 12);
                put("12131415", 4);
            }
        };

        for (String k : data.keySet()) {
            assertEquals(data.get(k), day.part2(List.of(k)));
        }
    }
    
    @Test
    public void part_two() {
        assertEquals(950, (int)day.part2(input.getLines()));
    }
}
