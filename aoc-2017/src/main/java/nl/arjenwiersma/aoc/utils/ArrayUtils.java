package nl.arjenwiersma.aoc.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * ArrayUtils
 */
public class ArrayUtils {
    /**
     * Partition an array bij interleaving it with itself given an offset.
     *
     * @param data   the list of integers to partition.
     * @param offset the offset to partition to.
     * @param wrap   consider the last partition to be with the first element.
     */
    public static List<List<Integer>> interleave(List<Integer> data, int offset, boolean wrap) {
        List<List<Integer>> result = new ArrayList<>();

        int max = wrap ? data.size() : data.size() - 1;

        for (int i = 0, j = offset; i < max; i++, j++) {
            j = j % data.size();

            result.add(List.of(data.get(i), data.get(j)));
        }

        return result;
    }

    public static char[][] rotateClockWise(char[][] matrix) {
        int size = matrix.length;
        char[][] ret = new char[size][size];

        for (int i = 0; i < size; ++i)
            for (int j = 0; j < size; ++j)
                ret[i][j] = matrix[size - j - 1][i]; // ***

        return ret;
    }

    public static void flip(char[] array) {
        int left = 0;
        int right = array.length - 1;
        while (left < right) {
            char temp = array[left];
            array[left] = array[right];
            array[right] = temp;
            ++left;
            --right;
        }
    }

    public static void flip(char[][] rows) {
        for (char[] row : rows) {
            flip(row);
        }
    }
}
