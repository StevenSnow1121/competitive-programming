package nl.arjenwiersma.aoc.days;

import java.util.Arrays;
import java.util.List;

import nl.arjenwiersma.aoc.common.Day;
import nl.arjenwiersma.aoc.utils.ArrayUtils;

public class Day01 extends Day<List<Integer>, Integer> {
    @Override
    public List<Integer> parseInput(List<String> input) {
        String in = input.get(0);
        return Arrays.stream(in.split(""))
            .mapToInt(Integer::parseInt)
            .boxed()
            .toList();
    }

    @Override
    public Integer solver1(List<Integer> input) {
        return ArrayUtils.interleave(input,1,true).stream()
            .filter(x -> x.get(0) == x.get(1)).mapToInt(x -> x.get(0)).sum();
    }

    @Override
    public Integer solver2(List<Integer> input) {
        return ArrayUtils.interleave(input, input.size()/2, true).stream()
            .filter(x -> x.get(0) == x.get(1)).mapToInt(x -> x.get(0)).sum();
    }
}
