package nl.arjenwiersma.aoc.days;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import nl.arjenwiersma.aoc.common.Day;

/**
 * Day16
 */
public class Day16 extends Day<String[],String> {
    String programs = "abcdefghijklmnop";

    public void setPrograms(String programs) {
        this.programs = programs;
    }

    @Override
    public String[] parseInput(List<String> input) {
        return input.get(0).split(",");
    }

    protected String spin(String in, int amount) {
        return in.substring(in.length() - amount)
            + in.substring(0, in.length() - amount);        
    }

    protected String partner(String in, String[] params) {
        int[] indexes = new int[] {in.indexOf(params[0]), in.indexOf(params[1])};

        return swap(in, indexes);
    }

    private String swap(String in, int[] indexes) {
        Arrays.sort(indexes);
        StringBuilder sb = new StringBuilder();
        if (indexes[0] > 0) {
            sb.append(in.substring(0, indexes[0]));
        }
        sb.append(in.charAt(indexes[1]));
        sb.append(in.substring(indexes[0] + 1, indexes[1]));
        sb.append(in.charAt(indexes[0]));
        if (indexes[1] < in.length()) {
            sb.append(in.substring(indexes[1] + 1));
        }
        return sb.toString();
    }
    

    private String dance(String config, String[] input) {
        for (String s : input) {
            if (s.startsWith("s")) {
                int amount = Integer.parseInt(s.substring(1));
                config = spin(config, amount);
            } else {
                String[] params = s.substring(1).split("/");
                switch (s.charAt(0)) {
                case 'p' -> {
                    config = partner(config, params);
                }
                case 'x' -> {
                    int[] p = Arrays.stream(params).mapToInt(Integer::parseInt).toArray();
                    config = swap(config, p);
                }
                }
            }
        }
        return config;
    }

    @Override
    public String solver1(String[] input) {
        return dance(programs, input);
    }

    @Override
    public String solver2(String[] input) {
        List<String> seen = new ArrayList<>();
        int iter = 1000000000;
        for (int index = 0; index < iter; index++) {
            programs = dance(programs, input);
            if (!seen.contains(programs)) {
                seen.add(programs);
            } else {
                /**
                 * From here the cycle will just continue, so no need to calculate everything.
                 * Take the modulo of the billion with the index of the last one (size - 1), from
                 * there it is the result steps to 1 billion
                 */
                break;
            }
        }
        return seen.get(iter % seen.size() - 1);
    }
}
