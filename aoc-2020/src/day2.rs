use regex::Regex;

#[derive(Debug)]
pub struct Password(usize, usize, char, String);


#[aoc_generator(day2)]
pub fn input_generator(input: &str) -> Vec<Password> {
    let re = Regex::new(r"^(\d+)-(\d+)\s(.+): (.+)$").unwrap();
    let e: Vec<Password> = input.lines().map(|l| {
        let caps = re.captures(l).unwrap(); 
        Password (
            caps.get(1).map(|m| m.as_str().parse().unwrap()).unwrap(),
            caps.get(2).map(|m| m.as_str().parse().unwrap()).unwrap(),
            caps.get(3).map(|m| m.as_str().chars().next().unwrap()).unwrap(),
            caps.get(4).map(|m| m.as_str().to_string()).unwrap(),
        )
    }).collect();

    e
}

#[aoc(day2, part1)]
pub fn part1(input: &[Password]) -> u32 {
    let mut counter = 0;

    for p in input {
        let c = p.3.chars().filter(|&x| p.2 == x).count();
        if c >= p.0 && c <= p.1 {
            counter += 1;
        }
    }
    
    counter                     // 580
}

#[aoc(day2, part2)]
pub fn part2(input: &[Password]) -> u32 {
    let mut counter = 0;

    for p in input {
        let c1 = p.3.chars().nth(p.0-1).unwrap();
        let c2 = p.3.chars().nth(p.1-1).unwrap();

        if (c1 == p.2 || c2 == p.2) && c1 != c2 {
            counter += 1;
        }
    }
    
    counter                     // 611
}


#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn part1_example() {
        let i = input_generator("1-3 a: abcde\n1-3 b: cdefg\n2-9 c: ccccccccc");

        assert_eq!(part1(&i), 2);
        
    }

    #[test]
    fn part2_example() {
        let i = input_generator("1-3 a: abcde\n1-3 b: cdefg\n2-9 c: ccccccccc");

        assert_eq!(part2(&i), 1);
    }
}
