#!/usr/bin/env bash

YEAR=$1
DAY=$2

# https://github.com/scarvalhojr/aoc-cli
which aoc 2>/dev/null || exit

mkdir -p aoc-$YEAR/src/main/java/nl/arjenwiersma/aoc/days
touch aoc-$YEAR/src/main/java/nl/arjenwiersma/aoc/days/Day$(printf %02d $DAY).java
mkdir -p aoc-$YEAR/src/main/resources
touch aoc-$YEAR/src/main/resources/Day$(printf %02d $DAY).txt
aoc r -d $DAY -y $YEAR > aoc-$YEAR/src/main/resources/Day$(printf %02d $DAY).txt
mkdir -p aoc-$YEAR/src/test/java/nl/arjenwiersma/aoc/days
touch aoc-$YEAR/src/test/java/nl/arjenwiersma/aoc/days/Day$(printf %02d $DAY)Test.java
mkdir -p aoc-$YEAR/src/test/resources
aoc d -d $DAY -y $YEAR  --file aoc-$YEAR/src/test/resources/input-$(printf %02d $DAY).txt
