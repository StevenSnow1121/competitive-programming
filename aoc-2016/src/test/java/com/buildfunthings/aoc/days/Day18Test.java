package com.buildfunthings.aoc.days;

import static org.junit.Assert.assertEquals;

import com.buildfunthings.aoc.common.Day;
import com.buildfunthings.aoc.common.DayInputExternalResource;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

public class Day18Test {
    @Rule
    public DayInputExternalResource input = new DayInputExternalResource(18);

    Day<Long> day;
    
    @Before
    public void before() {
        day = new Day18();
    }

    @Test
    public void part1() {
        assertEquals(1913, (long)day.part1(input.getLines()));
    }
    
    @Test
    public void part2() {
        assertEquals(19993564, (long)day.part2(input.getLines()));
    }
}
