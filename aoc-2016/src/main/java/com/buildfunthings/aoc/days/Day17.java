package com.buildfunthings.aoc.days;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.PriorityQueue;

import javax.xml.bind.DatatypeConverter;

import com.buildfunthings.aoc.common.Day;

public class Day17 implements Day<String> {

    public String produceMD5(String passcode, String path) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");

            String a = passcode + path;
            md.update(a.getBytes());
            byte[] digest = md.digest();

            return DatatypeConverter.printHexBinary(digest).toLowerCase();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return null;
    }

    public boolean[] doorStates(String hash) {
        boolean[] doors = new boolean[4];

        // up, down, left, right
        for (int i = 0; i < 4; i++) {
            doors[i] = hash.charAt(i) > 97 ? true : false;
        }

        return doors;
    }

    public record Entry (int x, int y, String path) {}

    public String findPath(String passcode, boolean first) {
        // priority queue, take shortest path first (meaning that if we find a path we can stop)
        PriorityQueue<Entry> queue = new PriorityQueue<>((a,b) -> Integer.compare(a.path.length(),b.path.length()));

        queue.add(new Entry(0, 0, ""));

        String maxPath = null;
        while (!queue.isEmpty()) {
            // take an entry and generate all new possible paths
            Entry e = queue.poll();

            if (e.x == 3 && e.y == 3) {
                if (first) return e.path;
                maxPath = e.path;
                continue;
            } 

            String hash = produceMD5(passcode, e.path);
            boolean[] doors = doorStates(hash);

            // can we move through an open door?
            // up, down, left, right
            if (doors[0] && e.y != 0) queue.add(new Entry(e.x,e.y-1,e.path + "U"));
            if (doors[1] && e.y != 3) queue.add(new Entry(e.x,e.y+1,e.path + "D"));
            if (doors[2] && e.x != 0) queue.add(new Entry(e.x-1,e.y,e.path + "L"));
            if (doors[3] && e.x != 3) queue.add(new Entry(e.x+1,e.y,e.path + "R"));
        }
        return maxPath; // should be unreachable
    }
    
    @Override
    public String part1(List<String> input) {
        String passcode = input.get(0);

        return findPath(passcode, true);
    }

    @Override
    public String part2(List<String> input) {
        String passcode = input.get(0);

        return "" + findPath(passcode, false).length();
    }

}
